﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClassLibrary1;
using BAL;
using pagging.Models;

namespace pagging.Controllers
{
    public class paggingController : Controller
    {
        
        // GET: pagging
        public ActionResult getstudent()
        {
            viewmodelstudent vmstd = new viewmodelstudent();
            vmstd.currentpage = 1;
            vmstd.pagesize = 2;
            studentbal sbal = new studentbal();
            vmstd.student = sbal.getstudent(1, 2);
            
             var totalrecordcount= sbal.getcount();
            vmstd.totalpagecount = (totalrecordcount % vmstd.pagesize)==0?
                (totalrecordcount / vmstd.pagesize): (totalrecordcount / vmstd.pagesize)+1;
                       
            //var totalpages = (totalrecordcount / pagesize);                     
            return View(vmstd);
        }

        public ActionResult getstudent1(int currentpage,int pagesize)
        {
            viewmodelstudent vmstd = new viewmodelstudent();
            vmstd.currentpage = currentpage;
            vmstd.pagesize = pagesize;
            studentbal sbal = new studentbal();
            vmstd.student = sbal.getstudent1(currentpage, pagesize);
            var totalrecordcount = sbal.getcount();
            vmstd.totalpagecount = (totalrecordcount % vmstd.pagesize) == 0 ?
             (totalrecordcount / vmstd.pagesize) : (totalrecordcount / vmstd.pagesize) + 1;    
            return View("getstudent",vmstd);
        }
    }
}