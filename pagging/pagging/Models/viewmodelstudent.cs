﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClassLibrary1;

namespace pagging.Models
{
    public class viewmodelstudent
    {
        public List<student> student { get; set; }
        public int currentpage { get; set; }
        public int pagesize { get; set; }
        public int totalpagecount { get; set; }
    }
}