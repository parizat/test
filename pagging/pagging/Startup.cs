﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(pagging.Startup))]
namespace pagging
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
