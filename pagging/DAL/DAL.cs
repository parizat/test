﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;
using System.Data.SqlClient;
using System.Configuration;

namespace DAL
{
    public class studentdal
    {
        public int getcount()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings
                ["Db_student"].ConnectionString);
            SqlCommand cmd = new SqlCommand("Select count(*) from student", con);
            con.Open();
            int result = Convert.ToInt32( cmd.ExecuteScalar());
            con.Close();
            return result;
        }

        public List<student> getstudent(int currentpage, int pagesize)
        {
            List<student> lst = new List<student>();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings
                ["Db_student"].ConnectionString);
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@currentpage", currentpage);
            param[1] = new SqlParameter("@pagesize", pagesize);
            SqlCommand cmd = new SqlCommand("usp_getstudent", con);
            cmd.Parameters.AddRange(param);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            con.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while(dr.Read())
            {
                student std = new student();
                std.Id = Convert.ToInt32(dr["Id"]);
                std.Name = Convert.ToString(dr["Name"]);
                std.Gender = Convert.ToString(dr["Gender"]);
                std.DOB = Convert.ToDateTime(dr["DOB"]);
                std.Email = Convert.ToString(dr["Email"]);
                std.Course = Convert.ToString(dr["Course"]);
                lst.Add(std);
            }
            dr.Close();
            con.Close();
            return lst;
        }
    }
}
